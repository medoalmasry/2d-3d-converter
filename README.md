## Name
2D-3D-Converter

## Description
A docker image with FastAPI that serves as an end to end pipeline for 3D automatic reconstruction using [colmap](https://colmap.github.io/).

The predict function takes as input, a single video, and returns as output a reconstructed version of the object within the video.

## Badges
[![GitLab last commit](https://img.shields.io/gitlab/last-commit/medoalmasry/2d-3d-converter)](https://gitlab.com/medoalmasry/2d-3d-converter/-/commits/main)
[![GitLab all issues](https://img.shields.io/gitlab/issues/all/medoalmasry/2d-3d-converter)](https://gitlab.com/medoalmasry/2d-3d-converter/-/issues)
[![GitLab forks](https://img.shields.io/gitlab/forks/medoalmasry/2d-3d-converter)](https://gitlab.com/medoalmasry/2d-3d-converter/-/forks)

<!-- ## Visuals
![Visual Demo](https://gitlab.com/medoalmasry/arabic-ocr/-/raw/337f66c52248081da8da854f90189b032ac5f057/resources/demo.gif) -->

## Pre-requisites
* [Docker](https://docs.docker.com/engine/install/)
* [Cog](https://github.com/replicate/cog)
* [Nvidia Container](https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/latest/install-guide.html) (GPU only)

Identify your GPU name & architecture
```
nvidia-smi --query-gpu=gpu_name --format=csv
```
Check cuda arch of your gpu [here](https://developer.nvidia.com/cuda-gpus) and set cuda arch in [cog.yaml](https://gitlab.com/medoalmasry/2d-3d-converter/-/blob/main/cog.yaml?ref_type=heads#L52)

## Installation
```
git clone git@gitlab.com:medoalmasry/2d-3d-converter.git
cd 2d-3d-converter
cog build -t 2d3d-converter
```

## Usage

### Using Cog (for local files)
```
cog predict -i src_video=@VID.mp4 -i method="colmap"
cog predict -i src_video=@VID.mp4 -i method="colmap" -i quality="high"
```
quality can be either "low" "medium" "high" "extreme" but the default is "low"

### Using Docker (for remote files)
```
docker run -d --rm -p 5000:5000 --gpus all 2d3d-converter (with GPU support)

curl http://localhost:5000/predictions -X POST /
-H 'Content-Type: application/json' /
-d '{"input": {"src_video": "https://...", "method": "colmap", "quality": "high"}}'
```
* The reponse will be a json string that needs to be reconstructed into a .ply file that can be viewed using [MeshLab](https://www.meshlab.net/)
* Demo is available [here](https://gitlab.com/medoalmasry/2d-3d-converter/-/blob/main/demo/demo.ipynb)

## Debugging

### Using Cog
```
cog debug
```
### Using Docker
```
docker run -it --rm -p 5000:5000 --gpus all 2d3d-converter bash (with GPU support)
```

## Authors & acknowledgment
<a href="https://gitlab.com/medoalmasry"><img src="https://gitlab.com/uploads/-/system/user/avatar/13834902/avatar.png?width=64"></a>

## License
BSD 3-Clause License 

## Project status
CLOSED
