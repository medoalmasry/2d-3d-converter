import os
import shutil
import subprocess
import trimesh
import json
from cog import BasePredictor, File, Input, Path

class Predictor(BasePredictor):
    def setup(self):
        """Initialize here everything you need, such as loading the model into memory"""
        """Load the model into memory to make running multiple predictions efficient"""
        pass

    # Define the arguments and types the model takes as input
    def predict(self, 
                src_video: Path = Input(description="Video to reconstruct from"),
                method: str = Input(description="Colmap"),
                quality: str = Input(default="low", description="Quality of reconstruction")
        ) -> str:
        """Run reconstruction algorithm on the input video and return the path of the result"""

        # Create new file name
        new_file = "src_video"+os.path.splitext(src_video)[1]

        # Check if the input video starts with https:// or http:// and download it if it does
        if src_video.name.startswith("https://") or src_video.name.startswith("http://"):
            command = ['curl', src_video.name, '>', new_file]
            curl = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
            curl.wait()
            if curl.returncode == 0:
                print('CURL ran successfully')
            else:
                print('CURL failed with error code', curl.returncode)
        
        # Create Workspace Folder
        folder_path = os.path.join(os.getcwd(),"data")
        os.makedirs(folder_path, exist_ok=True)

        # Move video to workspace
        # takes the video from the default /tmp folder and moves it to the workspace folder
        shutil.copyfile(src_video, os.path.join(folder_path,new_file))

        # Create Images Folder
        images_path = os.path.join(folder_path,"images")
        os.makedirs(images_path, exist_ok=True)

        # Extract Images from Video
        command = ['ffmpeg', '-i', os.path.join("data",new_file), '-pix_fmt', 'pal8', '-qscale:v', '1', '-qmin', '1', '-vf', 'fps=1', os.path.join(images_path,'%04d.jpg')]
        ffmpeg = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        ffmpeg.wait()
        if ffmpeg.returncode == 0:
            print('FFMPEG ran successfully')
        else:
            print('FFMPEG failed with error code', ffmpeg.returncode)

        # Create Masks Folder
        masks_path = os.path.join(folder_path,"masks")
        os.makedirs(masks_path, exist_ok=True)

        # Extract Masks from Images
        command = ['rembg', 'p', '-om', images_path, masks_path, '-m', 'u2net_custom', '-x', '{"model_path": "./u2net.onnx"}']
        rembg = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        rembg.wait()
        if rembg.returncode == 0:
            print('REMBG ran successfully')
        else:
            print('REMBG failed with error code', rembg.returncode)

        # Run Colmap on Images and Masks
        command = ['colmap', 'automatic_reconstructor', '--workspace_path', folder_path, '--image_path', images_path, '--mask_path', masks_path, '--quality', quality]
        colmap = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)

        # Print the output while the process is running
        for line in iter(colmap.stdout.readline, b''):
            print(f"\r{line.decode('utf-8')}", end="")
        print("\n")

        # Wait for the process to finish and get the return code
        colmap.wait()
        colmap.stdout.close()
        if colmap.returncode == 0:
            print('COLMAP ran successfully')
        else:
            print('COLMAP failed with error code', colmap.returncode)

        # Locate ply file
        ply_path = os.path.join(folder_path,"dense/0/fused.ply")
        meshed_file_path = os.path.join(folder_path,'dense/0/meshed-poisson.ply') 

        # Run mesher on exported scene
        command = ['colmap', 'poisson_mesher', '--input_path', ply_path, '--output_path', meshed_file_path]
        mesher = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        mesher.wait()
        if mesher.returncode == 0:
            print('Mesher ran successfully')
        else:
            print('Mesher failed with error code', mesher.returncode)

        # Convert ply to string
        mesh = trimesh.load_mesh(meshed_file_path)
        
        try:
            dict_mesh = mesh.export(file_type='dict')
            mesh_str = json.dumps(dict_mesh)
        except AttributeError:
            mesh_str = "Mesh has no faces"

        # Return ply file as a string
        return mesh_str